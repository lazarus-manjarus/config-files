# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import os
from libqtile import bar, layout, widget
from libqtile.widget import Wlan
from libqtile.config import Click, Drag, Group, Key, Match, Screen, Key, KeyChord
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
import subprocess

mod = "mod4"
terminal = guess_terminal()
browser = 'firefox'
fileManager = 'thunar'
barSize = 25

keys = [
  #CHANGING FOCUS BETWEEN WINDOWS

    Key(
        [mod], "h",
        lazy.layout.left(),
        desc="Move focus to left"
        ),

    Key(
        [mod], "l",
        lazy.layout.right(),
        desc="Move focus to right"
        ),

    Key(
        [mod], "j",
        lazy.layout.down(),
        desc="Move focus down"
        ),

    Key(
        [mod], "k",
        lazy.layout.up(),
        desc="Move focus up"
        ),

    Key(
        [mod], "space",
        lazy.layout.next(),
        desc="Move window focus to other window"
        ),

    #MOVING WINDOWS

    Key(
        [mod, "shift"],"h",
        lazy.layout.shuffle_left(),
        desc="Move window to the left"
        ),

    Key(
        [mod, "shift"], "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right"
        ),

    Key(
        [mod, "shift"], "j",
        lazy.layout.shuffle_down(),
        desc="Move window down"
        ),

    Key(
        [mod, "shift"], "k",
        lazy.layout.shuffle_up(),
        desc="Move window up"
        ),

    #GROWING WINDOWS

    Key(
        [mod, "control"], "h",
        lazy.layout.grow_left(),
        desc="Grow window to the left"
        ),

    Key(
        [mod, "control"], "l",
        lazy.layout.grow_right(),
        desc="Grow window to the right"
        ),

    Key(
        [mod, "control"], "j",
        lazy.layout.grow_down(),
        desc="Grow window down"
        ),

    Key(
        [mod, "control"], "k",
        lazy.layout.grow_up(),
        desc="Grow window up"
        ),

    Key(
        [mod], "n",
        lazy.layout.normalize(),
        desc="Reset all window sizes"
        ),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"], "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),

    #VOLUME
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),


    #LAUNCHING APPS

    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "b", lazy.spawn(browser), desc="Launch brave"),
    Key([mod], "f", lazy.spawn(fileManager), desc="Launch file manager"),

    #CHANGING LAYOUTS - KILLING,SHUTDOWNING,QUITING,RELOADING

    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], "x", lazy.shutdown(), desc="Quiting Qtile"),
    Key([mod, "control"], "f", lazy.window.toggle_fullscreen(),desc="Toggle fullscreen"),
]

groups = []

group_names = ["0","1","2","3","4",]

group_labels = ["α","θ","Σ","λ","Ω",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            label=group_labels[i],
        ))
for i in groups:
    keys.extend(
        [
           #CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod], "Tab", lazy.screen.next_group()),
        Key([mod, "shift" ], "Tab", lazy.screen.prev_group()),
        Key(["mod1"], "Tab", lazy.screen.next_group()),
        Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        #Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
        ]
    )

#color
maincolor = '999999'#Dark Green Gotham:028482#Grey:343030#Red:660000,990000
maincolorlight = '000000'
maincolordarker = '000000'
maincolordarkest = '444444'

#font
def_font = "ubuntu bold"

layouts = [
        layout.Columns(border_normal=maincolorlight,border_focus=maincolor,border_focus_stack=maincolorlight,margin = 8, border_width=4,border_on_single=True),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    layout.MonadTall(border_normal=maincolorlight,border_focus=maincolor,border_focus_stack=maincolorlight,margin = 8, border_width=4,border_on_single=True),
    layout.MonadWide(border_normal=maincolorlight,border_focus=maincolor,border_focus_stack=maincolorlight,margin = 8, border_width=4,border_on_single=True),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    layout.Zoomy(),
]

widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=5,
)

extension_defaults = widget_defaults.copy()

screens = [
        Screen(
        wallpaper="~/.config/qtile/walls/wallpaper.jpg",
        wallpaper_mode='fill',
        top=bar.Bar(
            [
              widget.Sep(
                        linewidth= 0,
                        padding = 10,
                        foreground = "000000",
                        background = maincolor
                    ),

              widget.Image(
                        filename = '~/.config/qtile/icons/start.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('thunar')}
                        ), 

              widget.WidgetBox(
                  widgets = [

                     widget.Sep(
                        linewidth= 0,
                        padding = 20,
                        foreground = "000000",
                        background = maincolor
                    ),

                    widget.Image(
                        filename = '~/.config/qtile/icons/vmware.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('vmplayer')}
                        ), 

                    widget.Sep(
                        linewidth= 0,
                        padding = 10,
                        foreground = "000000",
                        background = maincolor
                    ),

 
                    widget.Image(
                        filename = '~/.config/qtile/icons/netbeans.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('netbeans')}
                        ), 

                    widget.Sep(
                        linewidth= 0,
                        padding = 10,
                        foreground = "000000",
                        background = maincolor
                    ),
                    
                    widget.Image(
                        filename = '~/.config/qtile/icons/vscode.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('code')}
                        ), 

                    widget.Sep(
                        linewidth= 0,
                        padding = 7,
                        foreground = "000000",
                        background = maincolor
                    ),

 
                    widget.Image(
                        filename = '~/.config/qtile/icons/whatsapp.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('whatsapp-for-linux')}
                        ), 

                    widget.Image(
                        filename = '~/.config/qtile/icons/teams.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('teams-for-linux')}
                        ), 
                     
                    widget.Image(
                        filename = "~/.config/qtile/icons/firefox.png",
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('firefox')},
                    ),

                    widget.Sep(
                        linewidth= 0,
                        padding = 10,
                        foreground = "000000",
                        background = maincolor
                    ),

                    widget.Image(
                        filename = "~/.config/qtile/icons/thunderbird.png",
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('thunderbird')},
                    ),

                    widget.TextBox(fmt='◗',fontsize=barSize+30,padding=-6,foreground=maincolor,background=maincolor),
                    
 
                    ],
                  text_closed = '❱',
                  text_open = '❰' ,
                  fontsize = barSize + 10,
                  foreground = maincolordarkest,
                  background = maincolor,
                  close_button_location = 'left',
                  ),
                widget.TextBox(fmt='◗',fontsize=barSize+30,padding=-6,foreground=maincolor,background = maincolordarkest),

                widget.GroupBox(rounded = True,fontsize = 20,background = maincolordarkest,foreground = maincolor ,inactive = maincolor,this_current_screen_border = maincolor,other_current_screen_border = maincolor,this_screen_border = "000000",font = def_font),
                
                widget.TextBox(fmt='◗',fontsize=barSize+30,padding=-6,foreground=maincolordarkest),

                widget.TaskList(borderwidth=0,font=def_font,foreground=maincolor,),

                widget.Spacer(background=maincolorlight,),

                widget.TextBox(fmt='◖',fontsize=barSize+30,padding=-6,foreground=maincolordarkest),
                widget.WidgetBox(
                        widgets = [
                            widget.Image(
                                filename = "~/.config/qtile/icons/shutdown.png",
                                iconsize=20,
                                background = maincolordarkest,
                                mouse_callbacks = {'Button1' : lambda : os.system('shutdown now')},
                            ),
                            #widget.Mpd2(background = maincolordarkest),
                            widget.Battery(format = '{percent:2.0%}',background = maincolordarkest),
                    ],
                        foreground = maincolor,
                        background = maincolordarkest,
                        fontsize = barSize + 10,
                        text_closed = '❰',
                        text_open = '❱',
                        close_button_location = 'right',
                        ),
                widget.BatteryIcon(update_interval=1,background=maincolordarkest),
   
                widget.Volume(padding=0,foreground="#ffffff",fmt="     🔊:{}     ",background=maincolordarkest,font = def_font),
                widget.TextBox(fmt='◗',fontsize=barSize+30,padding=-6,foreground=maincolordarkest,background=maincolordarker),
                widget.Wlan(interface = 'wlp3s0',background = maincolordarker,format='  {essid} {percent:2.0%}  ',disconnected_message = 'X',font = def_font),
                widget.TextBox(fmt='◖',fontsize=barSize+30,padding=-6,foreground=maincolor,background = maincolordarker),
                widget.Clock(format="  %a / %d-%m-%Y / %H:%M  ",foreground = "ffffff",background = maincolor,padding = -3,font = def_font),


            ],
            
            barSize,
            opacity=0.85,
             border_width=[0, 0, 2, 0],  # Draw top and bottom borders
             border_color=["000000", "000000", maincolor, "000000"]  # Borders are grey
        ),
    ),
Screen(
        wallpaper="~/.config/qtile/walls/wallpaper.jpg",
        wallpaper_mode='fill',
        top=bar.Bar(
            [
              widget.Sep(
                        linewidth= 0,
                        padding = 10,
                        foreground = "000000",
                        background = maincolor
                    ),

              widget.Image(
                        filename = '~/.config/qtile/icons/start.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('thunar')},
                        ), 

              widget.WidgetBox(
                  widgets = [
                      
                       widget.Sep(
                        linewidth= 0,
                        padding = 20,
                        foreground = "000000",
                        background = maincolor
                    ),

                     widget.Image(
                        filename = '~/.config/qtile/icons/vmware.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('vmplayer')}
                        ), 

                    widget.Sep(
                        linewidth= 0,
                        padding = 10,
                        foreground = "000000",
                        background = maincolor
                    ),


                    widget.Image(
                        filename = '~/.config/qtile/icons/netbeans.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('netbeans')}
                        ), 


                    widget.Sep(
                        linewidth= 0,
                        padding = 10,
                        foreground = "000000",
                        background = maincolor
                    ),

 
                    widget.Image(
                        filename = '~/.config/qtile/icons/vscode.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('codium')}
                        ), 

                    widget.Sep(
                        linewidth= 0,
                        padding = 7,
                        foreground = "000000",
                        background = maincolor
                    ),

 
                    widget.Image(
                        filename = '~/.config/qtile/icons/whatsapp.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('whatsapp-for-linux')}
                        ), 

                    widget.Image(
                        filename = '~/.config/qtile/icons/teams.png',
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('teams-for-linux')}
                        ), 
                     
                    widget.Image(
                        filename = "~/.config/qtile/icons/firefox.png",
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('firefox')},
                    ),

                    widget.Sep(
                        linewidth= 0,
                        padding = 10,
                        foreground = "000000",
                        background = maincolor
                    ),

                    widget.Image(
                        filename = "~/.config/qtile/icons/thunderbird.png",
                        background = maincolor,
                        mouse_callbacks = {'Button1' : lazy.spawn('thunderbird')},
                    ),

                    widget.TextBox(fmt='◗',fontsize=barSize+30,padding=-6,foreground=maincolor,background=maincolor),
                    
 
                    ],
                  text_closed = '❱',
                  text_open = '❰' ,
                  fontsize = barSize + 10,
                  foreground = maincolordarkest,
                  background = maincolor,
                  close_button_location = 'left',
                  ),
                widget.TextBox(fmt='◗',fontsize=barSize+30,padding=-6,foreground=maincolor,background = maincolordarkest),

                widget.GroupBox(rounded = True,fontsize = 20,background = maincolordarkest,foreground = maincolor ,inactive = maincolor,this_current_screen_border = maincolor,other_current_screen_border = maincolor,this_screen_border = "000000",font = def_font),
                
                widget.TextBox(fmt='◗',fontsize=barSize+30,padding=-6,foreground=maincolordarkest),

                widget.TaskList(borderwidth=0,font=def_font,foreground=maincolor,),

                widget.Spacer(background=maincolorlight,),

                widget.TextBox(fmt='◖',fontsize=barSize+30,padding=-6,foreground=maincolordarkest),
                widget.WidgetBox(
                        widgets = [
                            widget.Image(
                                filename = "~/.config/qtile/icons/shutdown.png",
                                iconsize=20,
                                background = maincolordarkest,
                                mouse_callbacks = {'Button1' : lambda : os.system('shutdown now')},
                            ),
                            widget.Battery(format = '{percent:2.0%}',background = maincolordarkest),
                    ],
                        foreground = maincolor,
                        background = maincolordarkest,
                        fontsize = barSize + 10,
                        text_closed = '❰',
                        text_open = '❱',
                        close_button_location = 'right',
                        ),
                widget.BatteryIcon(update_interval=1,background=maincolordarkest),
   
                widget.Volume(padding=0,foreground="#ffffff",fmt="     🔊:{}     ",background=maincolordarkest,font = def_font),
                widget.TextBox(fmt='◗',fontsize=barSize+30,padding=-6,foreground=maincolordarkest,background=maincolordarker),
                widget.Wlan(interface = 'wlp3s0',background = maincolordarker,format='  {essid} {percent:2.0%}  ',disconnected_message = 'X',font = def_font),
                widget.TextBox(fmt='◖',fontsize=barSize+30,padding=-6,foreground=maincolor,background = maincolordarker),
                widget.Clock(format="  %a / %d-%m-%Y / %H:%M  ",foreground = "ffffff",background = maincolor,padding = -3,font = def_font),


            ],
            barSize,
            
            opacity=0.85,
             border_width=[0, 0, 2, 0],  # Draw top and bottom borders
             border_color=["000000", "000000", maincolor, "000000"]  # Borders are grey
        ),
    ),

  ]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button4", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
